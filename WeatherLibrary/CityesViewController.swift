//
//  CityesViewController.swift
//  WeatherLibrary
//
//  Created by WSR on 10/7/19.
//  Copyright © 2019 WSR. All rights reserved.
//

import UIKit

class CityesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //Ссылка на таблицу (привязка)
    @IBOutlet weak var cityTable: UITableView!
    
    //Массив с городами
    var cityArray = [String] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // подписываемся на делегат (действия в таблице) и datasourse заполнение таблицы
        cityTable.delegate = self
        cityTable.dataSource = self
        // Do any additional setup after loading the view.
    }
    // количество ячеек
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //количество элементов в массиве
        return cityArray.count
        
    }
    // отрисовка ячеек
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        //Создаем переменную с ячейкой. Важно указать свой индификатор ячейки из сториборда
        let cell = cityTable.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell
        
        //присваиваем текст стандартному Label из массива по индексу
        cell.textLabel?.text = cityArray[indexPath.row]
        return cell
    }
    
    @IBAction func addnewCity(_ sender: Any) {
    
    
        let alert = UIAlertController(title: "Добавление города", message: "Введите пожалуйста Ваш город", preferredStyle: .alert)
        alert.addTextField { textField in
    textField.placeholder = "Город"
    }
    //создание действия ок с добавлением в массив содержимого поля для ввода
    let confirmAction = UIAlertAction (title: "OK", style: .default) { [weak alert] _ in
        //первое поле для ввода
        guard let textField = alert?.textFields?.first else { return }
        self.cityArray.append(textField.text!)
        self.cityTable.reloadData()
        
    }
    let canselAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    alert.addAction(confirmAction)
    alert.addAction(canselAction)
    present(alert, animated: true, completion: nil)

}
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.cityArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.cityTable.reloadData()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let selectCell = cityTable.indexPathForSelectedRow
        let vc = segue.destination as! ViewController
        vc.city = cityArray[selectCell!.row]
    }
}
